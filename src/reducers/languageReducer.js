// vim: set ts=2 sts=2 sw=2 et:
// @flow strict
//
// This file is part of OpenLifter, simple Powerlifting meet software.
// Copyright (C) 2019 The OpenPowerlifting Project.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import type { ChangeLanguageAction, OverwriteStoreAction } from "../types/actionTypes";
import type { LanguageState } from "../types/stateTypes";

type Action = ChangeLanguageAction | OverwriteStoreAction;

export default (state: LanguageState = "en", action: Action): LanguageState => {
  switch (action.type) {
    case "CHANGE_LANGUAGE":
      return action.language;

    case "OVERWRITE_STORE":
      return action.store.language;

    default:
      (action.type: empty); // eslint-disable-line
      return state;
  }
};
